import 'dart:io';

void main() {

  String? input_word = " Big black bug bit a big black dog on his big black nose ";
  var words = input_word.toLowerCase().split(" ");
  List<String> listofwords = [];
  List<int> wordcount = [];
  
  for (int i = 0; i < words.length; i++) {
    var repeatword = false;

    for (int j = 0; j < listofwords.length; j++) {
      if (words[i] == listofwords[j]) {
        repeatword = true;
        wordcount[j] = wordcount[j] + 1;
        break;
      }
    }
    if (repeatword == false) {
      listofwords.add(words[i]);
      wordcount.add(1);
    }
  }

  for (var i = 0; i < listofwords.length; i++) {
    print("${listofwords[i]} ${wordcount[i]}");
  }
}
  

